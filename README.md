# Lacmus EdgeYOLO: anchor-free, edge-friendly

Forked from [EdgeYOLO](https://github.com/LSH9832/edgeyolo)

**[Intro](#intro)**  

**[Quick Start](#quick-start)**  
- [setup](#setup)
- [docker run](#run-docker-)
- [detect](#detect)
- [train](#train)
- [evaluate](#evaluate)

**[Cite EdgeYOLO](#cite-edgeyolo)**   

**[Updates](#updates)**  

**[TODO](#todo)**  

## Intro

Project purpose is search for missing people in photos and video images collected with the help of the UAV.
Base object detection model is Yolo v5, improved with lite decoupled head and staged loss. 
Custom mosaic and mixup augmentations are optimized for small objects detection. 
Also, there are help tool to online view predictions and tensorrt converter.

## Quick Start

### setup

```shell
git clone https://gitlab.com/labintsev/lacmus_edge_yolo
cd lacmus_edge_yolo
pip install -r requirements.txt
```

For TensorRT model converter install [torch2trt](https://github.com/NVIDIA-AI-IOT/torch2trt).  

### run docker: 
```shell
docker run -it --rm -v .:/usr/src/edgeyolo --gpus device=1 edgeyolo:0.1
```

### detect

Original EdgeYOLO model visdrone trained weights available [here](https://github.com/LSH9832/edgeyolo/releases/tag/v0.0.0)

```shell
python src/detect.py --weights models/edgeyolo_visdrone.pth --source data/processed/test_visdrone/test/images --fp16
```

### train

First, prepare your dataset. For UAV images visdrone is preferred, so convert VOC to visdrone. 

```shell
python src/preprocess_data.py --out_visdrone test_visdrone --big_set Test
```

Second, create dataset config file (params/dataset/XXX.yaml).  

Third, edit training param file (params/train/train_XXX.yaml).  

Finally, start training:  

```shell
python src/train.py --config params/train/train_visdrone_lacmus_test.yaml 
```

### evaluate

```shell
python src/evaluate.py --weights models/edgeyolo_visdrone.pth --dataset params/dataset/lacmus-visdrone-test.yaml --device 0
```

## Cite EdgeYOLO

```
@article{edgeyolo2023,
  title={EdgeYOLO: An Edge-Real-Time Object Detector},
  author={Shihan Liu, Junlin Zha, Jian Sun, Zhuo Li, and Gang Wang},
  journal={arXiv preprint arXiv:2302.07483},
  year={2023}
}
```

## Updates

**[2023/4/27]** 
1. Adopted to ods mlops course.  


## TODO
- DVC pipelines
