import os
import click
from edgeyolo.data.voc_2_visdrone import load_img_and_annotations, crop_sample, save_annotations_to_file


@click.command()
@click.option('--in_voc', '-i', default='LADDv4')
@click.option('--out_visdrone', '-o', default='lacmus_visdrone')
@click.option('--big_set', '-b', default='Main')
@click.option('--crop_size', '-c', default=640)
@click.option('--width_crops', '-w', default=4)
@click.option('--height_crops', '-h', default=3)
def voc_2_visdrone(in_voc, out_visdrone, big_set, crop_size, width_crops, height_crops):
    in_data_dir = f'data/raw/{in_voc}'
    out_data_dir = f'data/processed/{out_visdrone}_{crop_size}_{width_crops}_{height_crops}'

    if os.path.exists(out_data_dir):
        os.system(f'rm -rf {out_data_dir}')
    os.mkdir(out_data_dir)

    make_subset('train', in_data_dir, out_data_dir, big_set, crop_size, width_crops, height_crops)
    make_subset('test', in_data_dir, out_data_dir, big_set, crop_size, width_crops, height_crops)


def data_gen(subset_indices, in_data_dir, crop_size, width_crops, height_crops):
    for idx in subset_indices:
        image, anns = load_img_and_annotations(idx, in_data_dir)
        crops = crop_sample(image, anns, crop_size, width_crops, height_crops, 4)
        for img_crop, anno_crop in crops:
            if len(anno_crop) > 0:
                yield img_crop, anno_crop


def make_subset(subset: str, in_data_dir: str, out_data_dir: str, big_set: str,
                crop_size: int, width_crops: int, height_crops: int):

    assert subset in {'train', 'test'}
    assert big_set in {'Main', 'Test'}

    idx_file_name = f'ImageSets/{big_set}/{subset}.txt'
    idx_path = os.path.join(in_data_dir, idx_file_name)
    with open(idx_path, encoding='utf-8') as f:
        subset_indices = f.readlines()

    subset_indices = [int(t.strip()) for t in subset_indices]

    subset_dir = os.path.join(out_data_dir, subset)
    os.mkdir(subset_dir)

    image_dir = os.path.join(subset_dir, 'images')
    annot_dir = os.path.join(subset_dir, 'annotations')
    os.mkdir(image_dir)
    os.mkdir(annot_dir)

    data_iter = iter(data_gen(subset_indices, in_data_dir, crop_size, width_crops, height_crops))

    for i, (img, annotations) in enumerate(data_iter):
        img_path = os.path.join(image_dir, f'{i}.jpg')
        img.save(img_path)
        save_annotations_to_file(i, annotations, annot_dir)


if __name__ == '__main__':
    voc_2_visdrone()
