"""
Script takes only config file, where defined dataset, model and hyperparams.
Training logs and models located in the output directory.
"""
import click
from edgeyolo import launch


@click.command()
@click.option('--config', '-d', default='params/train/train_visdrone_lacmus_test.yaml')
def train(config):
    launch(config)


if __name__ == '__main__':
    train()
