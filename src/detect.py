"""
Detect objects in images or video with specified model.
.. code-block:: console

  python pipelines/detect.py --weights ./models/edgeyolo_visdrone.pth
     --source /XX/XXX.mp4     # or dir with images, such as /dataset/coco2017/val2017
                                (jpg/jpeg, png, bmp, webp is available)
     --conf-thres 0.25
     --nms-thres 0.5
     --input-size 640 640
     --batch 1
     --save-dir ./output/detect/imgs    # if you press "s", the current frame will be saved in this dir
     --fp16
     --no-fuse                # do not re-parameterize model
     --no-label               # do not draw label with class name and confidence
     --mp                     # use multiprocess to show images more smoothly when batch > 1
     --fps 30                 # max fps limitation, valid only when option --mp is used

"""
import argparse

from edgeyolo.detect.detect_multi import detect_multi
from edgeyolo.detect.detect_single import detect_single


def get_args():
    parser = argparse.ArgumentParser("EdgeYOLO Detect parser")
    parser.add_argument("-w", "--weights", type=str, default="../models/edgeyolo_visdrone.pth", help="weight file")
    parser.add_argument("-s", "--source", type=str, default="../data/processed/test_visdrone/test/images",
                        help="video source or image dir")
    parser.add_argument("-c", "--conf-thres", type=float, default=0.25, help="confidence threshold")
    parser.add_argument("-n", "--nms-thres", type=float, default=0.55, help="nms threshold")
    parser.add_argument("--mp", action="store_true", help="use multi-process to accelerate total speed")
    parser.add_argument("--fp16", action="store_true", help="fp16")
    parser.add_argument("--no-fuse", action="store_true", help="do not fuse model")
    parser.add_argument("--input-size", type=int, nargs="+", default=[640, 640], help="input size: [height, width]")
    parser.add_argument("--trt", action="store_true", help="is trt model")
    parser.add_argument("--legacy", action="store_true", help="if img /= 255 while training, add this command.")
    parser.add_argument("--use-decoder", action="store_true", help="support original yolox model v0.2.0")
    parser.add_argument("--batch", type=int, default=1, help="batch size")
    parser.add_argument("--no-label", action="store_true", help="do not draw label")
    parser.add_argument("--save-dir", type=str, default="./output/detect/imgs/", help="image result save dir")
    parser.add_argument("--fps", type=int, default=99999, help="max fps")

    return parser.parse_args()


if __name__ == "__main__":
    arguments = get_args()
    if arguments.mp:
        detect_multi(arguments)
    else:
        detect_single(arguments)
