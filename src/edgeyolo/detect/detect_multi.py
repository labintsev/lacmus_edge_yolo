import os
from time import time
from multiprocessing import Process, Manager, freeze_support
from datetime import datetime as date

import cv2
import torch.cuda
from loguru import logger

from .detector_yolo import Detector
from .detector_trt import TRTDetector
from .detect_utils import DirCapture, draw


def inference(msg, results, args):
    detector = TRTDetector if args.trt else Detector
    detect = detector(
        weight_file=args.weights,
        conf_thres=args.conf_thres,
        nms_thres=args.nms_thres,
        input_size=args.input_size,
        fuse=not args.no_fuse,
        fp16=args.fp16,
        use_decoder=args.use_decoder,
    )
    if args.trt:
        args.batch = detect.batch_size

    # source loader setup
    if os.path.isdir(args.source):

        source = DirCapture(args.source)
        delay = 0
    else:
        source = cv2.VideoCapture(
            int(args.source) if args.source.isdigit() else args.source
        )
        delay = 1

    msg["class_names"] = detect.class_names
    msg["delay"] = delay

    success = True
    while source.isOpened() and success and not msg["end"]:
        frames = []
        for _ in range(args.batch):
            if msg["end"]:
                frames = []
                break
            success, frame = source.read()
            if not success:
                if not frames:
                    cv2.destroyAllWindows()
                    break
                while len(frames) < args.batch:
                    frames.append(frames[-1])
            else:
                frames.append(frame)

        if not frames:
            break

        results.put((frames, [r.cpu() for r in detect(frames, args.legacy)]))

    msg["end"] = True
    torch.cuda.empty_cache()
    msg["end_count"] += 1


def draw_imgs(msg, results, all_imgs, args):
    while "class_names" not in msg:
        pass
    class_names = msg["class_names"]

    while not msg["end"] or not results.empty():
        # print(len(msg["results"]))
        if not results.empty():
            for img in draw(
                *results.get(), class_names, 2, draw_label=not args.no_label
            ):
                all_imgs.put(img)
                # print(all_imgs.empty())
    torch.cuda.empty_cache()
    msg["end_count"] += 1


def show(msg, all_imgs, args):
    # import platform
    while "delay" not in msg:
        pass
    delay = msg["delay"]
    exist_save_dir = os.path.isdir(args.save_dir)

    all_dt = []
    t_0 = time()
    while not msg["end"] or not all_imgs.empty():
        if not all_imgs.empty():
            img = all_imgs.get()
            # print(img.shape)
            while time() - t_0 < 1.0 / args.fps - 0.0004:
                pass

            dt = time() - t_0
            all_dt.append(dt)
            if len(all_dt) > 300:
                all_dt = all_dt[-300:]

            mean_dt = sum(all_dt) / len(all_dt) * 1000
            print(
                f"\r{dt * 1000:.1f}ms --> {1. / dt:.1f}FPS, "
                f"average:{mean_dt:.1f}ms --> {1000. / mean_dt:.1f}FPS",
                end="      ",
            )

            t_0 = time()
            cv2.imshow("EdgeYOLO result", img)
            key = cv2.waitKey(delay)
            if key in [ord("q"), 27]:
                msg["end"] = True
                cv2.destroyAllWindows()
                break
            if key == ord(" "):
                delay = 1 - delay
            elif key == ord("s"):
                if not exist_save_dir:
                    os.makedirs(args.save_dir, exist_ok=True)
                file_name = str(date.now()).split('.', maxsplit=1)[0].replace(':', '').replace('-', '').replace(' ', '')
                img_name = f"{file_name}.jpg"
                full_path = os.path.join(args.save_dir, img_name)
                cv2.imwrite(full_path, img)
                logger.info(f"image saved to {file_name}.")

    print()
    print()
    torch.cuda.empty_cache()
    msg["end_count"] += 1

    while not msg["end_count"] == 3:
        pass


def detect_multi(args):
    freeze_support()

    shared_data = Manager().dict()
    shared_data["end"] = False
    shared_data["end_count"] = 0

    results = Manager().Queue()
    all_imgs = Manager().Queue()

    processes = [
        Process(target=inference, args=(shared_data, results, args)),
        Process(target=draw_imgs, args=(shared_data, results, all_imgs, args)),
        Process(target=show, args=(shared_data, all_imgs, args, os.getpid())),
    ]

    for process in processes:
        process.start()

    torch.cuda.empty_cache()

    for process in processes:
        process.join()

