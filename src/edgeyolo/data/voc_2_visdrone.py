import os
import xml.etree.ElementTree as et
from pathlib import Path
from typing import NamedTuple

import PIL
from PIL import ImageDraw
from PIL.JpegImagePlugin import JpegImageFile


class Rectangle(NamedTuple):
    """ Bounding box rectangle: (xmin, ymin) - (xmax, ymax)"""

    xmin: int
    ymin: int
    xmax: int
    ymax: int

    @property
    def w(self) -> int:
        """Width"""
        return self.xmax - self.xmin

    @property
    def h(self) -> int:
        """Height"""
        return self.ymax - self.ymin

    @property
    def square(self) -> float:
        """Square"""
        return self.w * self.h


class Annotation(NamedTuple):
    """Image annotation: class label, bounding box"""
    label: str
    bbox: Rectangle


class AnnotationFileReader:
    """Reading annotation file from LADD dataset (Pascal VOC)"""

    def __init__(self, filepath: str) -> None:
        self.filepath: Path = Path(filepath)

    def read_annotations(self) -> list[Annotation]:
        annotations = []
        root = et.parse(str(self.filepath)).getroot()
        for obj in root.iter('object'):
            bndbox = obj.find('bndbox')
            assert bndbox is not None
            annotation = Annotation(
                label=self._text(obj.find('name'), default=''),
                bbox=Rectangle(
                    xmin=int(float(self._text(bndbox.find('xmin'), default='0'))),
                    ymin=int(float(self._text(bndbox.find('ymin'), default='0'))),
                    xmax=int(float(self._text(bndbox.find('xmax'), default='0'))),
                    ymax=int(float(self._text(bndbox.find('ymax'), default='0'))),
                )
            )
            annotations.append(annotation)
        return annotations

    def _text(self, element: et.Element, default: str) -> str:
        if element is None:
            return default
        text = element.text
        if text is None:
            return default
        return text

    def __repr__(self) -> str:
        path = str(self.filepath)
        return f"AnnotationFile('{path}')"


def scale(src, x_factor, y_factor) -> Annotation:
    """Coordinates scaling by x and y factors"""
    return Annotation(
        label=src.label,
        bbox=Rectangle(
            xmin=round(src.bbox.xmin * x_factor),
            xmax=round(src.bbox.xmax * x_factor),
            ymin=round(src.bbox.ymin * y_factor),
            ymax=round(src.bbox.ymax * y_factor)
        )
    )


def shift(src, x_shift, y_shift) -> Annotation:
    """Coordinate shift by x and y pixels"""
    return Annotation(
        label=src.label,
        bbox=Rectangle(
            xmin=round(src.bbox.xmin - x_shift),
            xmax=round(src.bbox.xmax - x_shift),
            ymin=round(src.bbox.ymin - y_shift),
            ymax=round(src.bbox.ymax - y_shift)
        )
    )


def overlap_annotations(scaled_anns, left, top, right, bottom, crop_size) -> list:
    """Annotation and image crop overlapping"""
    crop_anns = []
    for ann in scaled_anns:
        if ann.bbox.xmin >= left and ann.bbox.ymin >= top:
            if ann.bbox.xmax <= right and ann.bbox.ymax <= bottom:
                crop_anns.append(shift(ann, left, top))
            elif ann.bbox.xmax - right < ann.bbox.w / 3 and ann.bbox.ymax - bottom < ann.bbox.h / 3:
                    crop_anns.append(Annotation(label=ann.label, bbox=Rectangle(
                        xmin=ann.bbox.xmin - left, ymin=ann.bbox.ymin - top,
                        xmax=min(crop_size, ann.bbox.xmax - left), ymax=min(crop_size, ann.bbox.ymax - top))))
    return crop_anns


def load_img_and_annotations(idx: int, voc_dir: str) -> tuple[JpegImageFile, list[Annotation]]:
    img_path = os.path.join(voc_dir, 'JPEGImages', f'{idx}.jpg')
    img = PIL.Image.open(img_path)
    ann_path = os.path.join(voc_dir, 'Annotations', f'{idx}.xml')
    anns = AnnotationFileReader(ann_path).read_annotations()
    return img, anns


def crop_sample(img, annotations, crop_size, width_crops, height_crops, top_n=4
                ) -> list[tuple[JpegImageFile, list[Annotation]]]:
    """Crop image for width_crops * height_crops,
    find new coordinates of annotations in every crop,
    resize every img to crop_size

    @return: top_n crops with maximum count of bounding boxes"""

    k_x = width_crops * crop_size / img.width
    k_y = height_crops * crop_size / img.height
    scaled_anns = [scale(a, k_x, k_y) for a in annotations]

    out = []
    img_r = img.resize(size=(width_crops * crop_size, height_crops * crop_size))
    for w in range(width_crops):
        for h in range(height_crops):
            left = w * crop_size
            top = h * crop_size
            right = (w + 1) * crop_size
            bottom = (h + 1) * crop_size

            crop_img = img_r.crop((left, top, right, bottom))
            crop_anns = overlap_annotations(scaled_anns, left, top, right, bottom, crop_size)
            out.append((crop_img, crop_anns))

    return sorted(out, key=lambda x: len(x[1]), reverse=True)[:top_n]


def save_annotations_to_file(idx: int, annotations: list[Annotation], out_dir: str = './') -> bool:
    """
    Save VOC annotations to VisDrone file
    @param idx: index of image and annotation file
    @param annotations: list of bboxes coordinates [left, top, width, height]
    @param out_dir: directory to save output file
    @return: True if saving is done.
    """
    out = []
    for ann in annotations:
        label = 1
        box_left = ann.bbox.xmin
        box_top = ann.bbox.ymin
        box_width = ann.bbox.xmax - box_left
        box_height = ann.bbox.ymax - box_top
        score, truncation, occlusion = 1, 0, 0
        out.append(
            f'{box_left},{box_top},{box_width},{box_height},\
{score},{label},{truncation},{occlusion}\n')

    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
    out_path = os.path.join(out_dir, f'{idx}.txt')

    with open(out_path, 'w', encoding='utf-8') as f:
        f.writelines(out)
    return True


def draw_boxes(i, image_dir, annot_dir) -> JpegImageFile:
    img_path = os.path.join(image_dir, f'{i}.jpg')
    img = PIL.Image.open(img_path)

    ann_path = os.path.join(annot_dir, f'{i}.txt')
    with open(ann_path, encoding='utf-8') as f:
        anns_txt_lines = f.readlines()

    bboxes = []
    for ann in anns_txt_lines:
        ann = ann.split(',')
        bboxes.append([int(coordinate) for coordinate in ann[:5]])

    draw = ImageDraw.Draw(img)
    for b in bboxes:
        draw.rectangle((b[0], b[1], b[0] + b[2], b[1] + b[3]), outline='red')
    return img
