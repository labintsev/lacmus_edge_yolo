Usage
=====

.. _installation:

Installation
-------------------
To use Edge Yolo, first create venv and install it dependencies.

.. code-block:: console
  $ python -m venv venv
  $ source venv/bin/activate
  (venv) $ pip install -r requirements.txt
