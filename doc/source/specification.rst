Specification
=============

Purpose
-------------

Purpose of Edge Yolo Lacmus is finding optimal training scheme, that provides the best recall score in pedestrian detecting task.

Tasks
---------
1. Adopt original Edge Yolo to Lacmus dataset with just one class.
2. Train Edge Yolo with several cropping scheme.

Results
-----------
1. Training result report.
2. Prediction api service with the best model