API
===

.. autosummary::
    :toctree: generated
    
    detect
    preprocess_data
    train
    evaluate
