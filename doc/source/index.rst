Welcome to Edge Yolo Lacmus's documentation!
============================================

**Edge Yolo Lacmus** is the project to find missing people in UAV images and videos.

.. note::
  This project is under active development.

Contents
-------------
.. toctree::
  usage
  specification
  data_format
  api

Check out the :doc:`usage` for futher information.
Installation :ref:`guide<installation>`.
